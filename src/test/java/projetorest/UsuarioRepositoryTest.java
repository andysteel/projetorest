package projetorest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gmail.andersoninfonet.projetorest.config.DataConfig;
import com.gmail.andersoninfonet.projetorest.model.Usuario;
import com.gmail.andersoninfonet.projetorest.repository.UsuarioRepository;
import com.gmail.andersoninfonet.projetorest.security.enums.PerfilEnum;
import com.gmail.andersoninfonet.projetorest.util.EncriptaUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy(
		@ContextConfiguration(classes = DataConfig.class)
)
public class UsuarioRepositoryTest {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Test
	public void deveInserirUmUsuario() {
		Usuario usuario = new Usuario();
		usuario.setEmail("andersoninfonet@gmail.com");
		usuario.setPerfil(PerfilEnum.ROLE_ADMIN);		
		usuario.setSenha(EncriptaUtil.encriptaSenha("123456"));
		
		Usuario user = usuarioRepository.save(usuario);
		Assert.assertNotNull(user);
	}
}
