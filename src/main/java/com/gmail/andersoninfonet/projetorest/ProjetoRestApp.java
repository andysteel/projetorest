package com.gmail.andersoninfonet.projetorest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ProjetoRestApp {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoRestApp.class, args);
	}
}
