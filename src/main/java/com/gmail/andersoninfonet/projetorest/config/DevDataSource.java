package com.gmail.andersoninfonet.projetorest.config;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariDataSource;

public class DevDataSource implements DataSourceConfig {

	
	public DataSource dataSource() { 
		HikariDataSource ds = null;
		try {
			ds = new HikariDataSource();
			ds.setDriverClassName("org.postgresql.Driver");
			ds.setJdbcUrl("jdbc:postgresql://localhost:5432/projetorest");
			ds.setUsername("root");
			ds.setPassword("502010");
			ds.setMaximumPoolSize(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ds;
	}

}
