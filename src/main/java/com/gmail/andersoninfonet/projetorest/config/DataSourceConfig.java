package com.gmail.andersoninfonet.projetorest.config;

import javax.sql.DataSource;

public interface DataSourceConfig {
	
	public DataSource dataSource();

}
