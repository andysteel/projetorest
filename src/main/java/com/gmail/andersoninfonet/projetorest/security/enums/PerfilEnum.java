package com.gmail.andersoninfonet.projetorest.security.enums;

public enum PerfilEnum {

	ROLE_ADMIN,
	ROLE_USUARIO;
}
