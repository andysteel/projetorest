package com.gmail.andersoninfonet.projetorest.security.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gmail.andersoninfonet.projetorest.model.Usuario;
import com.gmail.andersoninfonet.projetorest.security.JwtUserFactory;
import com.gmail.andersoninfonet.projetorest.service.UsuarioService;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException {
		Optional<Usuario> usuario = usuarioService.buscarPorEmail(username);
		if (usuario.isPresent()) {
			return JwtUserFactory.create(usuario.get());
		}
	
		throw new UsernameNotFoundException("Email não encontrado.");
	}

}
