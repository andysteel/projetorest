package com.gmail.andersoninfonet.projetorest.service;

import java.util.Optional;

import com.gmail.andersoninfonet.projetorest.model.Usuario;

public interface UsuarioService {

	Optional<Usuario> buscarPorEmail(String email);
}
