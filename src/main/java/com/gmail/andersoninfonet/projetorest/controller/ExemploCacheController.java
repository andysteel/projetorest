package com.gmail.andersoninfonet.projetorest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gmail.andersoninfonet.projetorest.service.ExemploCacheService;

@RestController
@RequestMapping("/api/v1/cache")
public class ExemploCacheController {

	@Autowired
	private ExemploCacheService service;
	
	@GetMapping
	public String exemplo() {
		return service.exemploCache();
	}
}
