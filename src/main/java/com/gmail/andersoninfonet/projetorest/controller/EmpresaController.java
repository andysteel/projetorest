package com.gmail.andersoninfonet.projetorest.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gmail.andersoninfonet.projetorest.dto.EmpresaDTO;
import com.gmail.andersoninfonet.projetorest.util.Response;

@RestController
@RequestMapping("/api/v1/empresas")
public class EmpresaController {

	@PostMapping
	public ResponseEntity<Response<EmpresaDTO>> cadastrar(@Valid @RequestBody EmpresaDTO dto, BindingResult result) {
		Response<EmpresaDTO> response = new Response<EmpresaDTO>();
		if(result.hasErrors()) {
			result.getAllErrors().forEach(error ->
				response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		dto.setId(1L);
		response.setData(dto);
		return ResponseEntity.ok(response);
	}
}
